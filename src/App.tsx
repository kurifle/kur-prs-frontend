import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import React, { useEffect, useGlobal, useState } from 'reactn'
import { backend } from './api/backend'
import { getSessionUser, setSessionUser } from './api/session'
import { getUser, getUserInfo, slackRedirectURI } from './api/slackoauth'
import CalendarPage from './components/pages/CalandarPage'
import DashboardPage from './components/pages/DashboardPage'
import HelpPage from './components/pages/HelpPage'
import NotFound from './components/pages/NotFound'
import SetChiefPage from './components/pages/SetChiefPage'
import SignInPage from './components/pages/SignInPage'

function App(): JSX.Element {
  const [isAuthenticated, setIsAuthenticated] = useGlobal('isAuthenticated')
  const [userID, setUserID] = useGlobal('userID')
  const [userName, setUserName] = useGlobal('userName')
  const [hasError, setHasError] = useState(false)

  // OAtuth
  useEffect(() => {
    ;(async () => {
      const params = new URLSearchParams(window.location.search)
      const code = params.get('code')
      if (code) {
        try {
          const resUser = await getUser(code)
          const resUserInfo = await getUserInfo(
            resUser.access_token,
            resUser.user_id
          )
          setSessionUser(
            resUser.access_token,
            resUser.user_id,
            resUserInfo.user?.profile?.display_name
          )
          window.location.href = slackRedirectURI
        } catch (err) {
          console.log(err)
          setHasError(true)
        }
      } else {
        const session = getSessionUser()
        if (session) {
          setIsAuthenticated(true)
          setUserID(session.user_id)
          setUserName(session.user_name)
        } else {
          setHasError(true)
        }
      }
    })()
  }, [])

  // Check if user is registered, then create user if not.
  useEffect(() => {
    // Function to create user
    const createUser = async () => {
      await backend
        .post('/users', {
          id: userID,
          name: userName,
          role: 'normal'
        })
        .catch((error) => {
          console.log(error.response)
        })
    }
    // Function to check user
    const chkUser = async () => {
      await backend
        .get(`/users/${userID}`)
        .then((res) => {
          const data = res.data
        })
        .catch((error) => {
          if (error.response.status == 404) {
            createUser()
          }
        })
    }
    chkUser()
  })

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          {!isAuthenticated || hasError ? (
            <SignInPage />
          ) : (
            <Redirect to="/dashboard" />
          )}
        </Route>
        <Route exact path="/dashboard">
          {!isAuthenticated || hasError ? <SignInPage /> : <DashboardPage />}
        </Route>
        <Route exact path="/calendar">
          {!isAuthenticated || hasError ? <SignInPage /> : <CalendarPage />}
        </Route>
        <Route exact path="/chief">
          {!isAuthenticated || hasError ? <SignInPage /> : <SetChiefPage />}
        </Route>
        <Route exact path="/help">
          {!isAuthenticated || hasError ? <SignInPage /> : <HelpPage />}
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}

export default App

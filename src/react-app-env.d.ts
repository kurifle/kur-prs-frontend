/// <reference types="react-scripts" />
import 'reactn'

declare module 'reactn/default' {
  export interface State {
    isAuthenticated: boolean
    userID: string
    userName: string
    changed: number
    mobileOpen: boolean
  }
}

declare type UserProps = {
  id: string
  name: string
  role: string
}

declare type EventProps = {
  id: number
  user_id: string
  title: string
  date: string
  slot: SlotIDs
  type: EventTypes
  memo: string
  user: UserProps
}

declare type EventTypes = 'practice' | 'instruction' | 'chief'

declare type SlotIDs = 1 | 2 | 3 | 4 | 5 | 6

declare type GenericTemplateProps = {
  fabEnabled: boolean
  children: React.ReactNode
}

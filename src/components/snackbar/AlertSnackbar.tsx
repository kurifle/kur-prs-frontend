import { Snackbar } from '@material-ui/core'
import { AlertTitle } from '@material-ui/lab'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'

const Alert = (props: AlertProps) => {
  return <MuiAlert elevation={6} variant="standard" {...props} />
}

const AlertSnackbar = ({
  title,
  message,
  open,
  severity,
  onClose
}: {
  title: string
  message: string
  open: boolean
  severity: 'error' | 'info' | 'success' | 'warning'
  onClose: () => void
}): JSX.Element => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center'
      }}
      open={open}
      autoHideDuration={2000}
      onClose={onClose}
    >
      <Alert onClose={onClose} severity={severity}>
        <AlertTitle>{title}</AlertTitle>
        {message}
      </Alert>
    </Snackbar>
  )
}

export default AlertSnackbar

import Fab, { FabProps } from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'

const FabAdd = (props: FabProps): JSX.Element => {
  return (
    <Fab {...props}>
      <AddIcon />
    </Fab>
  )
}

export default FabAdd

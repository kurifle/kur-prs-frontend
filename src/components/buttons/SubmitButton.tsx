import Button, { ButtonProps } from '@material-ui/core/Button'

const SubmitButton = (props: ButtonProps): JSX.Element => {
  return <Button {...props}>Submit</Button>
}

export default SubmitButton

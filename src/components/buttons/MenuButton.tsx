import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

const MenuButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <MenuIcon />
    </IconButton>
  )
}

export default MenuButton

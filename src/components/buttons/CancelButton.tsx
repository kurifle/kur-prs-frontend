import Button, { ButtonProps } from '@material-ui/core/Button'

const CancelButton = (props: ButtonProps): JSX.Element => {
  return <Button {...props}>Cancel</Button>
}

export default CancelButton

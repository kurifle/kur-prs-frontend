import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

const CloseIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <CloseIcon />
    </IconButton>
  )
}

export default CloseIconButton

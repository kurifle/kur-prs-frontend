import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import EditIcon from '@material-ui/icons/Edit'

const DeleteIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <EditIcon />
    </IconButton>
  )
}

export default DeleteIconButton

import Button, { ButtonProps } from '@material-ui/core/Button'

const SignOutButton = (props: ButtonProps): JSX.Element => {
  return <Button {...props}>Sign Out</Button>
}

export default SignOutButton

import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import MoreIcon from '@material-ui/icons/MoreVert'

const MoreButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <MoreIcon />
    </IconButton>
  )
}

export default MoreButton

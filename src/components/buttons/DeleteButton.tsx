import Button, { ButtonProps } from '@material-ui/core/Button'

const DeleteButton = (props: ButtonProps): JSX.Element => {
  return <Button {...props}>Delete</Button>
}

export default DeleteButton

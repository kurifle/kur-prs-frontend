import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'

const BeforeIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <NavigateBeforeIcon />
    </IconButton>
  )
}

export default BeforeIconButton

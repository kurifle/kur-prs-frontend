import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import CalendarTodayIcon from '@material-ui/icons/CalendarToday'

const CalendarIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <CalendarTodayIcon />
    </IconButton>
  )
}

export default CalendarIconButton

import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

const NextIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <NavigateNextIcon />
    </IconButton>
  )
}

export default NextIconButton

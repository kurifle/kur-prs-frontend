import IconButton, { IconButtonProps } from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'

const DeleteIconButton = (props: IconButtonProps): JSX.Element => {
  return (
    <IconButton {...props}>
      <DeleteIcon />
    </IconButton>
  )
}

export default DeleteIconButton

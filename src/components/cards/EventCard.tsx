import {
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider
} from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import clsx from 'clsx'
import { format } from 'date-fns'
import { Dispatch, SetStateAction } from 'react'
import React, { useEffect, useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { EventProps, EventTypes } from '../../react-app-env'
import { eventColor, slot2Time, str2Date } from '../../util'
import DeleteIconButton from '../buttons/DeleteIconButton'
import EditIconButton from '../buttons/EditIconButton'
import DeleteEventDialog from '../dialogs/DeleteEventDialog'
import EditEventDialog from '../dialogs/EditEventDialog'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: 'rotate(180deg)'
    },
    avatarPractice: {
      backgroundColor: eventColor('practice')
    },
    avatarInstruction: {
      backgroundColor: eventColor('instruction')
    },
    avatarChief: {
      backgroundColor: eventColor('chief')
    },
    bulletList: {
      paddingTop: 0,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      listStylePosition: 'inside',
      fontSize: theme.typography.body1.fontSize,
      color: theme.palette.text.secondary
    }
  })
)

const EventCard = (event: EventProps): JSX.Element => {
  const [changed] = useGlobal('changed')
  const [expanded, setExpanded] = useState(false)
  const [practice, setPractice] = useState<EventProps[]>([])
  const [instruction, setInstruction] = useState<EventProps[]>([])
  const [chief, setChief] = useState<EventProps[]>([])
  const [editDialogOpen, setEditDialogOpen] = useState<boolean>(false)
  const [delDialogOpen, setDelDialogOpen] = useState<boolean>(false)
  const classes = useStyles()

  const handleExpandClick = () => {
    setExpanded(!expanded)
  }

  const handleEditDialogOpen = () => {
    setEditDialogOpen(true)
  }

  const handleEditDialogClose = () => {
    setEditDialogOpen(false)
  }

  const handleDelDialogOpen = () => {
    setDelDialogOpen(true)
  }

  const handleDelDialogClose = () => {
    setDelDialogOpen(false)
  }

  const eventAvatar = (type: EventTypes) => {
    if (type == 'practice') {
      return <Avatar className={classes.avatarPractice}>練</Avatar>
    }
    if (type == 'instruction') {
      return <Avatar className={classes.avatarInstruction}>教</Avatar>
    }
    if (type == 'chief') {
      return <Avatar className={classes.avatarChief}>射</Avatar>
    }
  }

  useEffect(() => {
    const getEvent = async (
      type: string,
      setEventFunc: Dispatch<SetStateAction<EventProps[]>>
    ) => {
      await backend
        .get('/events', {
          params: {
            dfrom: event.date,
            dto: event.date,
            slot: event.slot,
            type: type
          }
        })
        .then((res) => {
          setEventFunc(res.data)
        })
        .catch((error) => console.log(error))
    }
    getEvent('practice', setPractice)
    getEvent('instruction', setInstruction)
    getEvent('chief', setChief)
  }, [changed])

  return (
    <Card className={classes.root}>
      <CardHeader
        titleTypographyProps={{ variant: 'h5', align: 'left' }}
        title={event.title}
        subheader={`${format(
          str2Date(event.date),
          'yyyy-MM-dd(ccc)'
        )} ${slot2Time(event.slot)}`}
        avatar={eventAvatar(event.type)}
      />
      <CardActions disableSpacing>
        <EditIconButton onClick={handleEditDialogOpen} />
        <EditEventDialog
          open={editDialogOpen}
          handleClose={handleEditDialogClose}
          event={event}
        />
        <DeleteIconButton onClick={handleDelDialogOpen} />
        <DeleteEventDialog
          open={delDialogOpen}
          handleClose={handleDelDialogClose}
          event={event}
        />
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Divider />
        <CardContent>
          <Typography paragraph color="textSecondary">
            練習:
            {practice.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography paragraph color="textSecondary">
            教習:
            {instruction.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography paragraph color="textSecondary">
            射場長:
            {chief.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography color="textSecondary">Memo:</Typography>
          <ul className={classes.bulletList}>
            {practice.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`eventcard-practice-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
            {instruction.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`eventcard-instruction-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
            {chief.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`eventcard-chief-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
          </ul>
        </CardContent>
      </Collapse>
    </Card>
  )
}

export default EventCard

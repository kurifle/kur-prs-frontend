import { Card, CardContent, CardHeader } from '@material-ui/core'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import AddIcon from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import { addDays, format } from 'date-fns'
import { Dispatch, SetStateAction } from 'react'
import React, { useEffect, useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { EventProps, SlotIDs } from '../../react-app-env'
import { slot2Time } from '../../util'
import MoreButton from '../buttons/MoreButton'
import AddEventDialog from '../dialogs/AddEventDialog'
import DeleteEventDialog from '../dialogs/DeleteEventDialog'
import EditEventDialog from '../dialogs/EditEventDialog'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275
    },
    bulletList: {
      paddingTop: 0,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
      listStylePosition: 'inside',
      fontSize: theme.typography.body1.fontSize
    }
  })
)

const SlotCard = ({
  date,
  slot
}: {
  date: Date
  slot: SlotIDs
}): JSX.Element => {
  const [userID] = useGlobal('userID')
  const [changed] = useGlobal('changed')
  const [userEvent, setUserEvent] = useState<EventProps | undefined>()
  const [practice, setPractice] = useState<EventProps[]>([])
  const [instruction, setInstruction] = useState<EventProps[]>([])
  const [chief, setChief] = useState<EventProps[]>([])
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null)
  const [addDialogOpen, setAddDialogOpen] = useState(false)
  const [editDialogOpen, setEditDialogOpen] = useState(false)
  const [delDialogOpen, setDelDialogOpen] = useState(false)
  const classes = useStyles()
  const dateStr = format(date, 'yyyy-MM-dd')
  const today = new Date()

  const handleMoreButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMoreClose = () => {
    setAnchorEl(null)
  }

  const handleAddDialogOpen = () => {
    setAddDialogOpen(true)
    handleMoreClose
  }

  const handleAddDialogClose = () => {
    setAddDialogOpen(false)
    handleMoreClose
  }

  const handleEditDialogOpen = () => {
    setEditDialogOpen(true)
    handleMoreClose
  }

  const handleEditDialogClose = () => {
    setEditDialogOpen(false)
    handleMoreClose
  }

  const handleDelDialogOpen = () => {
    setDelDialogOpen(true)
    handleMoreClose
  }

  const handleDelDialogClose = () => {
    setDelDialogOpen(false)
    handleMoreClose
  }

  useEffect(() => {
    const getEvent = async (
      type: string,
      setEventFunc: Dispatch<SetStateAction<EventProps[]>>
    ) => {
      await backend
        .get('/events', {
          params: {
            dfrom: dateStr,
            dto: dateStr,
            slot: slot,
            type: type
          }
        })
        .then((res) => {
          const data = res.data
          setEventFunc(data)
        })
        .catch((error) => console.log(error))
    }
    getEvent('practice', setPractice)
    getEvent('instruction', setInstruction)
    getEvent('chief', setChief)
  }, [changed])

  useEffect(() => {
    const getUserEvent = async () => {
      await backend
        .get('/events', {
          params: {
            user_id: userID,
            dfrom: dateStr,
            dto: dateStr,
            slot: slot
          }
        })
        .then((res) => {
          setUserEvent(res.data.shift())
        })
        .catch((error) => console.log(error))
    }
    getUserEvent()
  }, [changed])

  return (
    <div>
      <Card className={classes.root}>
        <CardHeader
          titleTypographyProps={{ variant: 'h6', align: 'left' }}
          title={slot2Time(slot)}
          subheader={`Slot ${slot}`}
          action={<MoreButton onClick={handleMoreButtonClick} />}
        />
        <Menu
          id="menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleMoreClose}
        >
          <MenuItem
            onClick={handleAddDialogOpen}
            disabled={userEvent != undefined || addDays(date, +1) < today}
          >
            <ListItemIcon>
              <AddIcon fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Reserve</Typography>
          </MenuItem>
          <MenuItem
            onClick={handleEditDialogOpen}
            disabled={userEvent == undefined || addDays(date, +1) < today}
          >
            <ListItemIcon>
              <EditIcon fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Edit</Typography>
          </MenuItem>
          <MenuItem
            onClick={handleDelDialogOpen}
            disabled={userEvent == undefined || addDays(date, +1) < today}
          >
            <ListItemIcon>
              <DeleteIcon fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Delete</Typography>
          </MenuItem>
        </Menu>
        <CardContent>
          <Typography paragraph>
            練習:
            {practice.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography paragraph>
            教習:
            {instruction.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography paragraph>
            射場長:
            {chief.map((event) => ' ' + event.user.name)}
          </Typography>
          <Typography paragraph>Memo:</Typography>
          <ul className={classes.bulletList}>
            {practice.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`slotcard-practice-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
            {instruction.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`slotcard-instruction-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
            {chief.map((event, i) => {
              if (event.memo.trim().length != 0) {
                return (
                  <li
                    key={`slotcard-chief-memo-${i}`}
                  >{`${event.memo} (${event.user.name})`}</li>
                )
              }
            })}
          </ul>
        </CardContent>
      </Card>
      {!userEvent && (
        <AddEventDialog
          open={addDialogOpen}
          handleClose={handleAddDialogClose}
          defaultDate={date}
          defaultSlot={slot}
        />
      )}
      {userEvent && (
        <EditEventDialog
          open={editDialogOpen}
          handleClose={handleEditDialogClose}
          event={userEvent}
        />
      )}
      {userEvent && (
        <DeleteEventDialog
          open={delDialogOpen}
          handleClose={handleDelDialogClose}
          event={userEvent}
        />
      )}
    </div>
  )
}

export default SlotCard

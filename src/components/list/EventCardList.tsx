import { Grid } from '@material-ui/core'
import { EventProps } from '../../react-app-env'
import EventCard from '../cards/EventCard'

const EventCardList = ({ events }: { events: EventProps[] }): JSX.Element => {
  return (
    <Grid
      container
      spacing={1}
      alignItems="stretch"
      justifyContent="flex-start"
    >
      {events.map((event) => (
        <Grid item key={`event-${event.id}`} xs={12} xl={6}>
          <EventCard {...event} />
        </Grid>
      ))}
    </Grid>
  )
}

export default EventCardList

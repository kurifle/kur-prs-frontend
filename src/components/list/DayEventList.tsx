import Grid from '@material-ui/core/Grid'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { format } from 'date-fns'
import { SlotIDs } from '../../react-app-env'
import SlotCard from '../cards/SlotCard'

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1
    }
  })
)

const DayEventList = ({ date }: { date: Date }): JSX.Element => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={2}
        alignItems="stretch"
        justifyContent="flex-start"
      >
        {[...Array(6)].map((x, i) => {
          return (
            <Grid
              item
              key={`slot-${format(date, 'yyyy-MM-dd')}-${i}`}
              xs={12}
              xl={6}
            >
              <SlotCard date={date} slot={(i + 1) as SlotIDs} />
            </Grid>
          )
        })}
      </Grid>
    </div>
  )
}
export default DayEventList

import AppBar from '@material-ui/core/AppBar'
import * as colors from '@material-ui/core/colors'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import {
  createStyles,
  createTheme,
  makeStyles,
  Theme
} from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import EventIcon from '@material-ui/icons/Event'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import HelpIcon from '@material-ui/icons/Help'
import HomeIcon from '@material-ui/icons/Home'
import SettingsIcon from '@material-ui/icons/Settings'
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount'
import { ThemeProvider } from '@material-ui/styles'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import React, { useGlobal } from 'reactn'
import { drawerWidth } from '../../config/uiconfig'
import { GenericTemplateProps } from '../../react-app-env'
import FabAdd from '../buttons/FabAdd'
import MenuButton from '../buttons/MenuButton'
import MoreButton from '../buttons/MoreButton'
import AddEventDialog from '../dialogs/AddEventDialog'
import SignOutDialog from '../dialogs/SignOutDialog'

const theme = createTheme({
  typography: {
    fontFamily: [
      'Noto Sans JP',
      'Lato',
      '游ゴシック Medium',
      '游ゴシック体',
      'Yu Gothic Medium',
      'YuGothic',
      'ヒラギノ角ゴ ProN',
      'Hiragino Kaku Gothic ProN',
      'メイリオ',
      'Meiryo',
      'ＭＳ Ｐゴシック',
      'MS PGothic',
      'sans-serif'
    ].join(',')
  },
  palette: {
    primary: {
      main: colors.blue[800]
    },
    text: {
      secondary: colors.grey[800]
    }
  }
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      top: 'auto',
      bottom: 0,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth
      }
    },
    menuButton: {
      marginRight: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        display: 'none'
      }
    },
    fabAdd: {
      position: 'absolute',
      zIndex: 1,
      top: -30,
      left: 0,
      right: 0,
      margin: '0 auto'
    },
    grow: {
      flexGrow: 1
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(2),
      paddingBottom: '75px',
      marginBottom: theme.spacing(2),
      overflow: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth
      }
    },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0
      }
    },
    drawerPaper: {
      width: drawerWidth
    },
    link: {
      textDecoration: 'none',
      color: theme.palette.text.primary
    },
    accountColumn: {
      width: drawerWidth,
      position: 'absolute',
      top: 'auto',
      bottom: 0
    }
  })
)

const GenericTemplate = (props: GenericTemplateProps): JSX.Element => {
  const [userID, setUserID] = useGlobal('userID')
  const [userName] = useGlobal('userName')
  const [mobileOpen, setMobileOpen] = useGlobal('mobileOpen')
  const [addDialogOpen, setAddDialogOpen] = useState<boolean>(false)
  const [signOutDialogOpen, setSignOutDialogOpen] = useState<boolean>(false)
  const classes = useStyles()

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen)
  }

  const handleAddDialogOpen = () => {
    setAddDialogOpen(true)
  }

  const handleAddDialogClose = () => {
    setAddDialogOpen(false)
  }

  const handleSignOutDialogOpen = () => {
    setSignOutDialogOpen(true)
  }

  const handleSignOutDialogClose = () => {
    setSignOutDialogOpen(false)
  }

  const drawer = (
    <div>
      <List>
        <Link to="/dashboard" className={classes.link}>
          <ListItem button>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
        </Link>
        <Link to="/calendar" className={classes.link}>
          <ListItem button>
            <ListItemIcon>
              <EventIcon />
            </ListItemIcon>
            <ListItemText primary="Calendar" />
          </ListItem>
        </Link>
        <Link to="/chief" className={classes.link}>
          <ListItem button>
            <ListItemIcon>
              <SupervisorAccountIcon />
            </ListItemIcon>
            <ListItemText primary="射場長設定" />
          </ListItem>
        </Link>
        <Link to="/help" className={classes.link}>
          <ListItem button>
            <ListItemIcon>
              <HelpIcon />
            </ListItemIcon>
            <ListItemText primary="Help" />
          </ListItem>
        </Link>
      </List>
      <div className={classes.accountColumn}>
        <Divider />
        <List>
          <ListItem>
            <ListItemIcon>
              <AccountCircleIcon />
            </ListItemIcon>
            <ListItemText primary={userName} />
          </ListItem>
          <ListItem button>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
          <ListItem button onClick={handleSignOutDialogOpen}>
            <ListItemIcon>
              <ExitToAppIcon />
            </ListItemIcon>
            <ListItemText primary="Sign Out" />
          </ListItem>
        </List>
      </div>
    </div>
  )

  const container =
    window !== undefined ? () => window.document.body : undefined

  return (
    <ThemeProvider theme={theme}>
      <div>
        <CssBaseline />
        <nav className={classes.drawer} aria-label="sidebar">
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper
              }}
              ModalProps={{
                keepMounted: true // Better open performance on mobile.
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <CssBaseline />
          {props.children}
        </main>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <MenuButton
              color="inherit"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            />
            {props.fabEnabled && (
              <div>
                <FabAdd
                  children={false}
                  color="secondary"
                  className={classes.fabAdd}
                  onClick={handleAddDialogOpen}
                />
                <AddEventDialog
                  open={addDialogOpen}
                  handleClose={handleAddDialogClose}
                  defaultDate={new Date()}
                  defaultSlot={1}
                />
              </div>
            )}
            <div className={classes.grow} />
            <MoreButton color="inherit" />
          </Toolbar>
        </AppBar>
        <SignOutDialog
          open={signOutDialogOpen}
          handleClose={handleSignOutDialogClose}
        />
      </div>
    </ThemeProvider>
  )
}

export default GenericTemplate

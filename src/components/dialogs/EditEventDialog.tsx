import DateFnsUtils from '@date-io/date-fns'
import AppBar from '@material-ui/core/AppBar'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import Slide from '@material-ui/core/Slide'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Toolbar from '@material-ui/core/Toolbar'
import { TransitionProps } from '@material-ui/core/transitions'
import Typography from '@material-ui/core/Typography'
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { addMonths } from 'date-fns'
import format from 'date-fns/format'
import { useEffect } from 'react'
import React, { forwardRef, useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { EventProps, EventTypes, SlotIDs } from '../../react-app-env'
import { slot2Time, str2Date } from '../../util'
import CloseIconButton from '../buttons/CloseIconButton'
import SubmitButton from '../buttons/SubmitButton'
import AlertSnackbar from '../snackbar/AlertSnackbar'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: 'relative'
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1
    },
    formControl: {
      margin: theme.spacing(2),
      minWidth: 200
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    },
    submitButton: {
      width: 120,
      margin: theme.spacing(1),
      marginRight: theme.spacing(2)
    }
  })
)

const Transition = forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />
})

const EditEventDialog = ({
  open,
  handleClose,
  event
}: {
  open: boolean
  handleClose: () => void
  event: EventProps
}): JSX.Element => {
  const [userID] = useGlobal('userID')
  const [changed, setChanged] = useGlobal('changed')
  const [date, setDate] = useState<Date | null>(str2Date(event.date))
  const [slot, setSlot] = useState<SlotIDs>(event.slot)
  const [type, setType] = useState<EventTypes>(event.type)
  const [memo, setMemo] = useState<string>(event.memo)
  const [snackOpen, setSnackOpen] = React.useState(false)
  const [snackTitle, setSnackTitle] = React.useState('')
  const [snackMessage, setSnackMessage] = React.useState('')
  const [alertSeverity, setAlertSeverity] = React.useState<
    'error' | 'info' | 'success' | 'warning'
  >('error')

  const classes = useStyles()

  const eventTypes = [
    { typeVal: 'practice', typeName: '練習' },
    { typeVal: 'instruction', typeName: '教習' },
    { typeVal: 'chief', typeName: '射場長' }
  ]

  const handleSnackOpen = () => {
    setSnackOpen(true)
  }

  const handleSnackClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return
    }
    setSnackOpen(false)
  }

  useEffect(() => {
    setDate(str2Date(event.date))
    setSlot(event.slot)
    setType(event.type)
    setMemo(event.memo)
  }, [open])

  const editEvent = async () => {
    const typeIndex = eventTypes.findIndex((x) => x.typeVal == type)
    await backend
      .put(`/events/${event.id}`, {
        user_id: userID,
        title: eventTypes[typeIndex].typeName,
        date: format(date!, 'yyyy-MM-dd'),
        slot: slot,
        type: type,
        memo: memo
      })
      .then((res) => {
        setSnackTitle('Success')
        setAlertSeverity('success')
        handleSnackOpen()
        setChanged(changed + 1)
      })
      .catch((error) => {
        console.log(error.response)
        setAlertSeverity('error')
        setSnackTitle(`${error.response.status}: ${error.response.statusText}`)
        setSnackMessage(`${error.response.data.detail}`)
        handleSnackOpen()
      })
  }

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              Edit
            </Typography>
            <CloseIconButton color="inherit" edge="end" onClick={handleClose} />
          </Toolbar>
        </AppBar>
        <FormControl className={classes.formControl}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              label="Date"
              orientation="portrait"
              openTo="date"
              format="yyyy-MM-dd(ccc)"
              value={date}
              onChange={setDate}
              minDate={new Date()}
              maxDate={addMonths(new Date(), +3)}
            />
          </MuiPickersUtilsProvider>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel id="slot-select-label">Slot</InputLabel>
          <Select
            labelId="slot-select-label"
            id="slot-select"
            value={slot}
            onChange={(changeEvent) => {
              setSlot(changeEvent.target.value as SlotIDs)
            }}
          >
            {[...Array(6)].map((x, i) => (
              <MenuItem
                value={i + 1}
                key={`edit-dialog-slot-select-${event.id}-menuitem-${i + 1}`}
              >
                {slot2Time(i + 1)}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel id="type-select-label">Type</InputLabel>
          <Select
            labelId="type-select-label"
            id="type-select"
            value={type}
            onChange={(changeEvent) => {
              setType(changeEvent.target.value as EventTypes)
            }}
          >
            {eventTypes.map((x) => (
              <MenuItem
                value={x.typeVal}
                key={`edit-dialog-type-select-${event.id}-menuitem-${x.typeVal}`}
              >
                {x.typeName}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl}>
          <TextField
            id="memo-text-field"
            label="Memo"
            defaultValue={event.memo}
            multiline
            maxRows={4}
            onChange={(changeEvent) => {
              setMemo(changeEvent.target.value as string)
            }}
          />
        </FormControl>

        <DialogActions>
          <SubmitButton
            variant="contained"
            type="submit"
            color="primary"
            size="medium"
            className={classes.submitButton}
            onClick={() => {
              editEvent()
              handleClose()
            }}
          />
        </DialogActions>
      </Dialog>
      <AlertSnackbar
        title={snackTitle}
        message={snackMessage}
        open={snackOpen}
        severity={alertSeverity}
        onClose={handleSnackClose}
      />
    </div>
  )
}

export default EditEventDialog

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { makeStyles, Theme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { format } from 'date-fns'
import { useGlobal } from 'reactn'
import { backend } from '../../api/backend'
import { EventProps } from '../../react-app-env'
import { slot2Time, str2Date } from '../../util'
import CancelButton from '../buttons/CancelButton'
import CloseIconButton from '../buttons/CloseIconButton'
import DeleteButton from '../buttons/DeleteButton'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
}))

const DeleteEventDialog = ({
  open,
  handleClose,
  event
}: {
  open: boolean
  handleClose: () => void
  event: EventProps
}): JSX.Element => {
  const [changed, setChanged] = useGlobal('changed')

  const classes = useStyles()

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="delete-events"
        maxWidth="md"
        fullWidth
      >
        <DialogTitle id="delete-event-dialog" className={classes.root}>
          <Typography variant="h6">本当に削除しますか?</Typography>
          <CloseIconButton
            className={classes.closeButton}
            onClick={handleClose}
          />
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="delete-event-description">
            <Typography>{event.title}</Typography>
            <Typography>{`${format(
              str2Date(event.date),
              'yyyy-MM-dd(ccc)'
            )} ${slot2Time(event.slot)}`}</Typography>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <CancelButton onClick={handleClose} color="default" autoFocus />
          <DeleteButton
            onClick={async () => {
              await backend
                .delete(`/events/${event.id}`)
                .then((res) => {
                  handleClose()
                  setChanged(changed + 1)
                })
                .catch((error) => {
                  console.log(error)
                })
            }}
            color="secondary"
          />
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default DeleteEventDialog

import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { makeStyles, Theme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { useGlobal } from 'reactn'
import { clearSessionUser } from '../../api/session'
import CancelButton from '../buttons/CancelButton'
import CloseIconButton from '../buttons/CloseIconButton'
import SignOutButton from '../buttons/SignOutButton'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  margin: {
    marginBottom: theme.spacing(2)
  }
}))

const SignOutDialog = ({
  open,
  handleClose
}: {
  open: boolean
  handleClose: () => void
}): JSX.Element => {
  const [isAuthenticated, setIsAuthenticated] = useGlobal('isAuthenticated')
  const [userID, setUserID] = useGlobal('userID')
  const [userName, setUserName] = useGlobal('userName')
  const classes = useStyles()

  const signOut = () => {
    clearSessionUser()
    setUserID('')
    setUserName('')
    setIsAuthenticated(false)
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="delete-events"
      maxWidth="md"
      fullWidth
    >
      <DialogTitle id="sign-out-dialog" className={classes.root}>
        <Typography variant="h6">サインアウトしますか?</Typography>
        <CloseIconButton
          className={classes.closeButton}
          onClick={handleClose}
        />
      </DialogTitle>
      <DialogContent>
        <SignOutButton
          variant="contained"
          color="secondary"
          fullWidth
          className={classes.margin}
          onClick={() => {
            signOut()
            handleClose()
          }}
        />
        <CancelButton
          variant="contained"
          color="default"
          fullWidth
          className={classes.margin}
          onClick={handleClose}
        />
      </DialogContent>
    </Dialog>
  )
}

export default SignOutDialog

import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import { slackLoginURI } from '../../api/slackoauth'
import '../../css/SignInPage.css'
import logo from '../../logo.svg'

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 5
  }
}))

const SignInPage = (): JSX.Element => {
  const classes = useStyles()
  return (
    <div className="SignInPage">
      <header className="SignIn-header">
        <img src={logo} className="logo" alt="logo" />
        <Typography
          variant="h4"
          color="textPrimary"
          align="center"
          className={classes.root}
        >
          Kyoto Univ.
        </Typography>
        <Typography
          variant="h4"
          color="textPrimary"
          align="center"
          className={classes.root}
        >
          Rifle Shooting Team
        </Typography>
        <Typography
          variant="h5"
          color="textPrimary"
          align="center"
          className={classes.root}
        >
          Practice Reservation System
        </Typography>
        <a
          className="SignIn-link"
          href={slackLoginURI}
          rel="noopener noreferrer"
        >
          Sign In with Slack
        </a>
      </header>
    </div>
  )
}

export default SignInPage

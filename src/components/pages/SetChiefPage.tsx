import DateFnsUtils from '@date-io/date-fns'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Tab from '@material-ui/core/Tab'
import Tabs from '@material-ui/core/Tabs'
import Typography from '@material-ui/core/Typography'
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date'
import { addMonths, addYears, format } from 'date-fns'
import React, { useEffect, useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { drawerWidth } from '../../config/uiconfig'
import { UserProps } from '../../react-app-env'
import { slot2Time } from '../../util'
import CancelButton from '../buttons/CancelButton'
import CloseIconButton from '../buttons/CloseIconButton'
import SubmitButton from '../buttons/SubmitButton'
import GenericTemplate from '../templates/GenericTemplate'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    appBar: {
      top: 0,
      bottom: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth
      }
    },
    tabsRoot: {
      justifyContent: 'center'
    },
    tabsScroller: {
      flexGrow: 0
    },
    tab: {
      [theme.breakpoints.down('sm')]: {
        minWidth: 50,
        Width: '10%'
      }
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      textAlign: 'center',
      paddingTop: theme.spacing(4)
    },
    formControl: {
      margin: theme.spacing(2),
      width: '80%',
      minWidth: 200
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    },
    submitButton: {
      width: 120,
      margin: theme.spacing(1),
      marginRight: theme.spacing(2)
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500]
    }
  })
)

const SetChiefPage = (): JSX.Element => {
  const [changed, setChanged] = useGlobal('changed')
  const [tabDayNum, setTabDayNum] = useState(0)
  const [chiefTable, setChiefTable] = useState(
    [...Array(7)].map(() => Array(6))
  )
  const [users, setUsers] = useState<UserProps[]>([])
  const [openDateRangeDialog, setOpenDateRangeDialog] = useState(false)
  const [dateFrom, setDateFrom] = useState<Date>(new Date())
  const [dateTo, setDateTo] = useState<Date>(addMonths(dateFrom, +1))
  const classes = useStyles()

  const setChief = (day: number, slot: number, chiefID: string | undefined) => {
    chiefTable[day][slot] = chiefID
  }

  const DayList = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

  const handleTabChange = (
    event: React.ChangeEvent<unknown>,
    newTabDayNum: number
  ) => {
    setTabDayNum(newTabDayNum)
  }

  const handleDialogOpen = () => {
    setOpenDateRangeDialog(true)
  }

  const handleDialogClose = () => {
    setOpenDateRangeDialog(false)
  }

  const changeDateFrom = (dfrom: MaterialUiPickersDate) => {
    setDateFrom(dfrom as Date)
    if ((dfrom as Date) > dateTo) {
      setDateTo(addMonths(dfrom as Date, +1))
    }
  }

  const changeDateTo = (dto: MaterialUiPickersDate) => {
    setDateTo(dto as Date)
  }

  const registerChief = () => {
    const submitChief = async (
      chiefID: string,
      weekday: number,
      slot: number
    ) => {
      await backend
        .post('/events/weekly', {
          user_id: chiefID,
          title: '射場長',
          type: 'chief',
          weekday: weekday,
          slot: slot,
          dfrom: format(dateFrom, 'yyyy-MM-dd'),
          dto: format(dateTo, 'yyyy-MM-dd')
        })
        .catch((error) => {
          console.log(error)
        })
    }
    chiefTable.map((timeTable, weekday) => {
      ;[...timeTable].map((chiefID, j) => {
        if (chiefID) {
          submitChief(chiefID, weekday, j + 1)
        }
      })
    })
    setChanged(changed + 1)
  }

  useEffect(() => {
    const getUsers = async () => {
      await backend
        .get('/users')
        .then((res) => {
          setUsers(res.data)
        })
        .catch((error) => {
          console.log(error)
        })
    }
    getUsers()
  }, [])

  return (
    <GenericTemplate fabEnabled={false}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" color="default" className={classes.appBar}>
          <Tabs
            value={tabDayNum}
            onChange={handleTabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            classes={{ root: classes.tabsRoot, scroller: classes.tabsScroller }}
          >
            {DayList.map((day) => (
              <Tab
                classes={{ root: classes.tab }}
                label={day}
                key={`tab-${day}`}
              />
            ))}
          </Tabs>
        </AppBar>
        <div className={classes.content}>
          <div className={classes.toolbar}>
            {chiefTable.map((timeTable, i) => (
              <div
                role="tabpanel"
                hidden={i !== tabDayNum}
                id={`day-tabpanel-${i}`}
                key={`day-tabpanel-${i}`}
                aria-labelledby={`day-tabpanel-${i}`}
              >
                {[...timeTable].map((chief, j) => (
                  <FormControl
                    className={classes.formControl}
                    key={`form-${tabDayNum}-${j}`}
                  >
                    <InputLabel id={`slot-${tabDayNum}-${j}-label`}>
                      {slot2Time(j + 1)}
                    </InputLabel>
                    <Select
                      labelId={`slot-${tabDayNum}-${j}-label`}
                      id={`slot-${tabDayNum}-${j}`}
                      key={`slot-${tabDayNum}-${j}`}
                      className={classes.selectEmpty}
                      value={chief}
                      onChange={(changeEvent) => {
                        setChief(
                          tabDayNum,
                          j,
                          changeEvent.target.value as string | undefined
                        )
                      }}
                    >
                      <MenuItem value={undefined}>{'None'}</MenuItem>
                      {users.map((user) => (
                        <MenuItem
                          value={user.id}
                          key={`menuitem-${tabDayNum}-${j}-${user.id}`}
                        >
                          {user.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                ))}
              </div>
            ))}
            <SubmitButton
              variant="contained"
              type="submit"
              color="primary"
              size="medium"
              className={classes.submitButton}
              onClick={handleDialogOpen}
            />
          </div>
        </div>
      </div>
      <Dialog
        open={openDateRangeDialog}
        onClose={handleDialogClose}
        aria-labelledby="submit-chief"
        maxWidth="md"
        fullWidth
      >
        <DialogTitle>
          <Typography variant="h6">期間</Typography>
          <CloseIconButton
            className={classes.closeButton}
            onClick={handleDialogClose}
          />
        </DialogTitle>
        <FormControl className={classes.formControl}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              label="From"
              orientation="portrait"
              openTo="date"
              format="yyyy-MM-dd(ccc)"
              value={dateFrom}
              onChange={changeDateFrom}
              minDate={new Date()}
              maxDate={addYears(new Date(), +1)}
            />
          </MuiPickersUtilsProvider>
        </FormControl>
        <FormControl className={classes.formControl}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              label="To"
              orientation="portrait"
              openTo="date"
              format="yyyy-MM-dd(ccc)"
              value={dateTo}
              onChange={changeDateTo}
              minDate={dateFrom}
              maxDate={addYears(dateFrom, +1)}
            />
          </MuiPickersUtilsProvider>
        </FormControl>
        <DialogActions>
          <CancelButton onClick={handleDialogClose} color="default" />
          <SubmitButton
            type="submit"
            color="primary"
            onClick={() => {
              registerChief()
              handleDialogClose()
            }}
          />
        </DialogActions>
      </Dialog>
    </GenericTemplate>
  )
}

export default SetChiefPage

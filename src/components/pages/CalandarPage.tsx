import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { addDays, addMonths, format } from 'date-fns'
import { useEffect } from 'react'
import React, { useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { drawerWidth } from '../../config/uiconfig'
import { EventProps } from '../../react-app-env'
import { eventColor } from '../../util'
import BeforeIconbutton from '../buttons/BeforeIconButton'
import CalendarIconButton from '../buttons/CalendarIconButton'
import NextIconButton from '../buttons/NextIconButton'
import DayEventList from '../list/DayEventList'
import GenericTemplate from '../templates/GenericTemplate'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    appBar: {
      top: 0,
      bottom: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth
      }
    },
    title: {
      flexGrow: 1,
      fontWeight: 'bold',
      textAlign: 'center'
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      paddingTop: theme.spacing(8)
    }
  })
)

const CalendarPage = (): JSX.Element => {
  const [userID] = useGlobal('userID')
  const [changed, setChanged] = useGlobal('changed')
  const [events, setEvents] = useState<EventProps[]>([])
  const [isMonthView, setIsMonthView] = useState<boolean>(true)
  const [date, setDate] = useState(new Date())
  const classes = useStyles()

  const toggleView = () => {
    setIsMonthView(!isMonthView)
  }

  useEffect(() => {
    const getMonthEvents = async () => {
      const today = new Date()
      const dfrom = format(addMonths(today, -3), 'yyyy-MM-dd')
      const dto = format(addMonths(today, +3), 'yyyy-MM-dd')
      await backend
        .get('/events', { params: { user_id: userID, dfrom: dfrom, dto: dto } })
        .then((res) => {
          setEvents(res.data)
        })
    }
    getMonthEvents()
  }, [changed])

  const contents = () => {
    if (isMonthView) {
      return (
        <div className={classes.root}>
          <FullCalendar
            plugins={[dayGridPlugin, interactionPlugin]}
            initialView="dayGridMonth"
            height="auto"
            titleFormat={{ year: 'numeric', month: 'short' }}
            events={events.map((event) => ({
              title: event.title,
              date: event.date,
              color: eventColor(event.type)
            }))}
            selectable={true}
            dateClick={(arg) => {
              toggleView()
              setDate(arg.date)
            }}
            eventClick={(arg) => {
              toggleView()
              setDate(arg.event.start!)
            }}
          />
        </div>
      )
    } else {
      return (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <BeforeIconbutton
                edge="start"
                color="inherit"
                onClick={() => {
                  setDate(addDays(date, -1))
                }}
              />
              <Typography variant="h5" className={classes.title}>
                {format(date, 'yyyy-MM-dd(ccc)')}
              </Typography>

              <CalendarIconButton
                edge="start"
                color="inherit"
                onClick={toggleView}
              />

              <NextIconButton
                edge="end"
                color="inherit"
                onClick={() => {
                  setDate(addDays(date, +1))
                }}
              />
            </Toolbar>
          </AppBar>
          <div className={classes.content}>
            <div className={classes.toolbar}>
              <DayEventList date={date} />
            </div>
          </div>
        </div>
      )
    }
  }

  return <GenericTemplate fabEnabled={false}>{contents()}</GenericTemplate>
}

export default CalendarPage

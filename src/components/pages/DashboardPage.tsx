import Typography from '@material-ui/core/Typography'
import { addMonths, format } from 'date-fns'
import React, { useEffect, useGlobal, useState } from 'reactn'
import { backend } from '../../api/backend'
import { EventProps } from '../../react-app-env'
import EventCardList from '../list/EventCardList'
import GenericTemplate from '../templates/GenericTemplate'

const DashboardPage = (): JSX.Element => {
  const [userID] = useGlobal('userID')
  const [changed, setChanged] = useGlobal('changed')
  const [events, setEvents] = useState<EventProps[]>([])

  useEffect(() => {
    const getEvents = async () => {
      const today = new Date()
      const dfrom = format(today, 'yyyy-MM-dd')
      const dto = format(addMonths(today, +1), 'yyyy-MM-dd')
      await backend
        .get('/events', { params: { user_id: userID, dfrom: dfrom, dto: dto } })
        .then((res) => {
          const data = res.data
          setEvents(data)
        })
    }
    getEvents()
  }, [changed])

  return (
    <GenericTemplate fabEnabled={true}>
      <Typography variant="h5" paragraph>
        Upcoming Events
      </Typography>
      <EventCardList events={events} />
    </GenericTemplate>
  )
}

export default DashboardPage

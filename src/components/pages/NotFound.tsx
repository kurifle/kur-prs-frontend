import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import React from 'react'
import '../../css/NotFound.css'
import logo from '../../logo.svg'

const useStyles = makeStyles(() => ({
  root: {
    paddingTop: 5
  }
}))

const NotFound = (): JSX.Element => {
  const classes = useStyles()
  return (
    <div className="">
      <header className="NotFound404">
        <img src={logo} className="logo" alt="logo" />
        <Typography
          variant="h4"
          color="textSecondary"
          align="center"
          className={classes.root}
        >
          404 Not Found
        </Typography>
        <a href="/" rel="noopener noreferrer">
          Home
        </a>
      </header>
    </div>
  )
}

export default NotFound

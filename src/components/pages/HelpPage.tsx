import { Typography } from '@material-ui/core'
import GenericTemplate from '../templates/GenericTemplate'

const HelpPage = (): JSX.Element => {
  return (
    <GenericTemplate fabEnabled={false}>
      <Typography variant="h4" component="h1">
        使い方
      </Typography>
    </GenericTemplate>
  )
}

export default HelpPage

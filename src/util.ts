import { blue, indigo, orange, teal } from '@material-ui/core/colors'
import { EventTypes } from './react-app-env'

export const str2Date = (dateStr: string): Date => {
  return new Date(dateStr)
}

export const slot2Time = (slot: number): string => {
  const timeTable = [
    '8:45-10:15',
    '10:30-12:00',
    '13:00-14:30',
    '14:45-16:15',
    '16:30-18:00',
    '18:00-20:00'
  ]
  return timeTable[slot - 1]
}

export const eventColor = (type: EventTypes): string => {
  if (type == 'practice') {
    return blue[500]
  }
  if (type == 'instruction') {
    return teal[500]
  }
  if (type == 'chief') {
    return orange[500]
  }
  return indigo[500]
}

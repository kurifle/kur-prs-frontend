const sessionStorage = window.sessionStorage

export const setSessionUser = (
  access_token: string | undefined,
  user_id: string | undefined,
  user_name: string | undefined
): void => {
  const sessionUser = JSON.stringify({ access_token, user_id, user_name })
  sessionStorage.setItem(process.env.REACT_APP_APP_NAME!, sessionUser)
}

export const getSessionUser = () => {
  const sessionUser = JSON.parse(
    sessionStorage.getItem(process.env.REACT_APP_APP_NAME!)!
  )
  return sessionUser!
}

export const clearSessionUser = (): void => {
  sessionStorage.removeItem(process.env.REACT_APP_APP_NAME!)
}

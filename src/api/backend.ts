import axios from 'axios'
import { getSessionUser } from './session'

const getHeadersInfo = () => {
  const sessionUser = getSessionUser()
  if (sessionUser) {
    return {
      'access-token': sessionUser.access_token,
      'user-id': sessionUser.user_id
    }
  } else {
    return {
      'access-token': '',
      'user-id': ''
    }
  }
}

export const backend = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_URI,
  headers: getHeadersInfo()
})

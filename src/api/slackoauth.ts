import {
  OauthAccessResponse,
  UsersInfoResponse,
  WebClient
} from '@slack/web-api'

export const getUser = (code: string): Promise<OauthAccessResponse> => {
  return new WebClient().oauth.access({
    client_id: process.env.REACT_APP_SLACK_CLIENT_ID!,
    client_secret: process.env.REACT_APP_SLACK_CLIENT_SECRET!,
    redirect_uri: process.env.REACT_APP_SLACK_REDIRECT_URI!,
    code: code
  })
}
export const getUserInfo = (
  access_token: string | undefined,
  user_id: string | undefined
): Promise<UsersInfoResponse> => {
  return new WebClient().users.info({
    token: access_token,
    user: user_id!
  })
}

export const slackRedirectURI = process.env.REACT_APP_SLACK_REDIRECT_URI!
export const slackLoginURI = `https://slack.com/oauth/authorize?client_id=${process.env.REACT_APP_SLACK_CLIENT_ID}&scope=identify&redirect_uri=${process.env.REACT_APP_SLACK_REDIRECT_URI}`

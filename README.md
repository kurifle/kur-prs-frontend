Practice Reservation System
===========================

**Under construction now**

Frontend of practice reservation system.

## Features
- Simple
- Using Slack OAuth

## License
Copyright (c) 2021 Takayuki YANO

The source code is licensed under the MIT License, see LICENSE.